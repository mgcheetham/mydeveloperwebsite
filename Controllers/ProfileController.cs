﻿using System;
using System.Web.Mvc;
using System.IO;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Configuration;

[assembly: InternalsVisibleTo("DeveloperTest")]

namespace MyDeveloperWebsite.Controllers
{
    public class ProfileController : Controller
    {
        private string apiKey = "";
        private string baseUrl = "";
        private string targetUrl = "";

        public ActionResult ProfilePage()
        {
            return View();
        }

        /// <summary>
        /// Begins converting profile for download
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> GetProfile()
        {
            GetHostedVariables();
            //GetLocalVariables();

            return Json(await CreateNewPDF());
        }

        /// <summary>
        /// Creates a new pdf from html page using sejda api
        /// </summary>
        /// <returns></returns>
        internal async Task<bool> CreateNewPDF()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation(
                    "Authorization", "Token: " + apiKey
                );

                var json = new JObject();
                json.Add("url", targetUrl);
                json.Add("viewportWidth", 1200);

                var content = new StringContent(json.ToString(), Encoding.UTF8, "application/json");

                using (var res = await client.PostAsync(baseUrl, content))
                {
                    var statusCode = (int)res.StatusCode;

                    if (statusCode == 200)
                    {
                        var httpStream = await res.Content.ReadAsStreamAsync();

                        var outputFilePath = Server.MapPath("~/Profile/matt_cheetham_profile.pdf");

                        using (var fileStream = System.IO.File.Create(outputFilePath))

                        using (var reader = new StreamReader(httpStream))
                        {
                            httpStream.CopyTo(fileStream);
                            fileStream.Flush();
                        }

                        return true;
                    }
                    else
                    {
                        Debug.WriteLine(statusCode);
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// Gets environment variable
        /// </summary>
        private void GetHostedVariables() {

            //apiKey = Environment.GetEnvironmentVariable("SEJDA");
            //baseUrl = Environment.GetEnvironmentVariable("BASE");
            //targetUrl = Environment.GetEnvironmentVariable("TARGET");

            apiKey = ConfigurationManager.AppSettings["SEJDA"];
            baseUrl = ConfigurationManager.AppSettings["BASE"];
            targetUrl = ConfigurationManager.AppSettings["TARGET"];
        }

        /// <summary>
        /// Gets environment variable
        /// </summary>
        private void GetLocalVariables()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            string path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            doc.Load(path + "\\source\\repos\\settings\\developer_site.xml");

            apiKey = doc.GetElementsByTagName("apiKey")[0].InnerText;
            baseUrl = doc.GetElementsByTagName("baseUrl")[0].InnerText;
            targetUrl = doc.GetElementsByTagName("targetUrl")[0].InnerText;
        }

        /// <summary>
        /// Old download process for generating a pdf
        /// </summary>
        internal void DownloadFile()
        {
            try
            {
                string path = Server.MapPath("~/Profile/matt_cheetham_profile.pdf");

                Response.Clear();
                Response.ClearHeaders();
                Response.ClearContent();

                Response.ContentType = ".pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=matt_cheetham_profile.pdf");
                Response.WriteFile(path);
                Response.End();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
