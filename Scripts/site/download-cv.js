﻿var DownloadCV = {

    fn: {

        // Ajax call to get profile
        BeginDownload: function () {

            Ajax.fn.ajaxPost(
                "/Profile/GetProfile",
                this.DownloadSuccess,
                this.DownloadFailure
            );
        },

        // Runs processes on outcome of ajax call
        DownloadSuccess: function (response) {

            if (response == true) DownloadCV.fn.CreateNewPDF();
            if (response == false) DownloadCV.fn.DisplayErrorPDF();
        },

        // Shows error if pdf generation failed
        DownloadFailure: function (response) {

            DownloadCV.fn.LoadingSpinner('hide');
            alert("Unexpected error downloading file");
        },

        // Creates a fake click for the hidden anchor download 
        CreateNewPDF: function () {

            setTimeout(function ()
            {
                DownloadCV.fn.LoadingSpinner('hide');
                const anchor = document.getElementById('download-anchor');
                anchor.click();
            }, 500);
        },

        // Shows and hides the spinner and download logo
        LoadingSpinner: function (status) {

            if (status == 'show') {
                document.getElementById('download-cv').style.visibility = 'hidden';
                document.getElementById('download-spinner').style.visibility = 'visible';
            }

            if (status == 'hide') {
                document.getElementById('download-cv').style.visibility = 'visible';
                document.getElementById('download-spinner').style.visibility = 'hidden';
            }
        },

        // Displays an error generating a new report
        DisplayErrorPDF: function () {

            DownloadCV.fn.LoadingSpinner('hide');
            alert("There was an error creating the pdf report.");
        }
    },

    initialise: function () {

        // Download profile on click starts process
        $("#download-cv").on("click", function () {

            DownloadCV.fn.LoadingSpinner('show');
            DownloadCV.fn.BeginDownload();
        });
    }
}
