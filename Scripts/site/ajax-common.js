﻿var Ajax = {

    vars: {

    },

    fn: {
        ajaxPost: function (postUrl, onSuccess, onError, data, passThroughData, iTimeoutMillis) {

            if (typeof iTimeoutMillis !== "number") {
                iTimeoutMillis = 30000;
            }
            if (typeof passThroughData === "undefined") {
                passThroughData = {};
            }
            if (typeof onSuccess !== "function") {
                onSuccess = function () { };
            }
            if (typeof onError !== "function") {
                onError = function () { };
            }
            if (typeof data === "undefined") {
                data = null;
            }

            var scsrfToken = $("input[name='__RequestVerificationToken']").val();

            if (typeof scsrfToken !== "string") {
                scsrfToken = "";
            }

            // Make ajax call
            $.ajax({
                type: "POST",
                url: postUrl,
                cache: false,
                data: data === null ? null : JSON.stringify(data),
                success: function (response, status, jqXhr) {

                    onSuccess(response, status, jqXhr, passThroughData);
                },
                error: function (jqXhr, status, errorName) {

                    // Handle generic errors if they exist
                    if (jqXhr.status === 401) {

                        // Unauthorised. Force a refresh
                        window.location.href = window.location.href;
                        return;
                    }
                    else if (status === "timeout") {

                        // Function call timeout

                    }

                    onError(jqXhr, status, errorName, passThroughData);
                },
                timeout: iTimeoutMillis,
            });
        },
    },
}